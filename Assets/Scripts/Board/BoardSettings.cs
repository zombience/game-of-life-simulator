﻿using UnityEngine;
using System.Collections;

/*
 * Boardsettings.cs is a helper class to organize settings in the inspector window in Unity
 * With this helper class, these variables can be folded, making settings more easily ignorable
 */
[System.Serializable]
public class PetriDish
{
	public int gridWidth;
	public int gameSpeed = 10; // How fast each generation is processed
	public int initialPercent = 20; // Percentage of board that will be covered with new cells on restart

	public float cellMaxSize; // How large is the maximum of each cell at full life
	public float boardScale; // Determines the spacing of each grid space. Interesting visual results when this is smaller than cellMaxSize

	public int boardSize { get { return gridWidth * gridWidth;}}
	public float cellSpeed { get { return (float)gameSpeed - ((float)gameSpeed * .3f);}} // Determines the speed at which cells physically grow to maximum size



}
