﻿using UnityEngine;
using System.Collections;

public class StateInfo 
{
	public bool phase;
	public bool isThreeDim;
	public bool reset;
	public bool isPlaying;
	public int steps;
	public int liveCells;

	public StateInfo()
	{
		// Initial settings
		phase = true;
		isThreeDim = false;
		reset = false;
		isPlaying = true;
		steps = 0;
		liveCells = 0;
	}

	//TODO: update all state scripts to reference manager.stateData rather than their own bools or integers

}
