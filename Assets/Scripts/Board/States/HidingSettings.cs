﻿using UnityEngine;
using System.Collections;


/*
 * HidingSettings.cs handles hiding the GUI after the user has determined to either restart the game or stop editing settings
 */
public class HidingSettings : GameBaseState 
{

	protected float hideTime = .7f;
	protected SettingsGUI gui;
	
	public override void Enter (GameManager m)
	{
		manager = m;
		gui = manager.settingsGUI;
		timer = Time.time + hideTime;
	}
	
	public override void Execute ()
	{
		float t = gui.curve.Evaluate((timer - Time.time) / hideTime);
		gui.Slide(t);
		if(t <= 0 && manager.stateInfo.reset)
		{
			if (!manager.stateInfo.isThreeDim)
				manager.ChangeState(new InitializeState());
			else
				manager.ChangeState(new Initialize3DState());
		}
		else if (t <= 0)
		{
			if (!manager.stateInfo.isThreeDim)
				manager.ChangeState(new PlayState());
			else
				manager.ChangeState(new Play3DState());
		}
	}
	
	public override void Exit ()
	{
		gui.settings.isVisible = false;	

	}
}
