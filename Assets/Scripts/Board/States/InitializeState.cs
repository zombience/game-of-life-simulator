using UnityEngine;
using System.Collections;

/*
 * InitializeState.cs is used to set up the board with new settings and cells 
 * It also exfoliates old cells 
 */
public class InitializeState : GameBaseState
{
	
	protected bool finished;

	#region state machine actions
	public override void Enter (GameManager m)
	{
		base.Enter(m);
		manager.StopAllCoroutines();
		manager.StartCoroutine(CreatePoints());
		manager.stateInfo.steps = 0;
		manager.floor.SetActive(true);
	}
	
	public override void Execute ()
	{
		if(finished)
			manager.ChangeState(new PlayState());
	}
	
	public override void Exit ()
	{
		manager.stateInfo.phase = true; // If we've reset the game, reset the phase as well
	}
	#endregion

	#region state specific actions
	protected virtual IEnumerator CreatePoints() 
	{
		// If resetting midgame, remove previous array of cells and destroy each cell
		if(manager.stateInfo.reset)
		{
			DestroyCells();
			manager.stateInfo.reset = false;
			manager.stateInfo.isThreeDim = false;
		}

		manager.cells = new Cell[manager.settings.boardSize];


		int index = 0;
		int w = manager.settings.gridWidth;


		// Set up cell array and assign settings
		for(int i = 0; i < w; i++)
		{
			for(int j = 0; j < w; j++)
			{
				GameObject obj = (GameObject)GameObject.Instantiate(manager.cellPrefab);
				GameObject.Destroy(obj.GetComponent<Cell3D>());
				Cell cell = obj.GetComponent<Cell>();
				manager.cells[index] = cell;	
				cell.transform.parent = manager.transform;
				cell.Initialize(obj.transform, i, j, manager.settings.boardScale, w, w, manager.settings.cellMaxSize) ;
				cell.index = index;

				cell.SetAdjacent(i, j);

				// Basic randomize. nothing fancy. 
				if(Random.Range (0, 100) < manager.settings.initialPercent)
					cell.SetState(true, true);

				index++;
			}
			yield return null; // Reduce the load for larger grids, also creates an incremental populating effect
		}

		finished = true;
	}

	protected virtual void DestroyCells()
	{
		DestroyCells(manager.cells);
		DestroyCells(manager.c3lls);
	}

	protected virtual void DestroyCells(Cell[] c)
	{
		if (c == null)
			return;
		for (int i = 0; i < manager.cells.Length; i++)
		{
			GameObject.Destroy(c[i].gameObject); // For future: pass reference rather than destroying and recreating
			GameObject.Destroy(c[i]);// = null;
		}
		manager.cells = null;
		
	}

	protected virtual void DestroyCells(Cell3D[] c)
	{
		if (c == null)
			return;

		for (int i = 0; i < manager.c3lls.Length; i++)
		{
			GameObject.Destroy(c[i].gameObject); // For future: pass reference rather than destroying and recreating
			//c[i] = null;
			GameObject.Destroy(c[i]);	  // Don't wait for GC, explicitly destroy
		}
		manager.c3lls = null;
	}
	#endregion
}
