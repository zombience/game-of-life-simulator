﻿using UnityEngine;
using System.Collections;

/*
 * PostEditCellsState.cs handles returning the camera back to "play" position
 * That's it
 */
public class PostEditCellsState : GameBaseState 
{

	public override void Enter (GameManager m)
	{
		manager = m;
	}
	
	public override void Execute ()
	{
		if(manager.cam.MoveToPlay())
			manager.ChangeState(new PlayState());
	}
	
	public override void Exit ()
	{
		manager.settingsGUI.settings.isLifeAltering = false; // We have just finished irrevocably changing the petri dish's life 
	}
}
