﻿using UnityEngine;
using System.Collections;

/*
 *  CellGraph.cs takes as input from PlayState.cs a percentage of cells currently alive
 *  Cellgraph moves a particle system to simulate a vertical seismograph-like look
 */
public class CellGraph : MonoBehaviour 
{

	public float distance; // Distance from resting that the graph can travel
	protected Vector3 origin;
	protected Vector3 destination;
	protected Transform trans;

	void Start ()
	{
		trans = transform;
		origin = trans.position;
		destination = origin;
		destination.x += distance;
		destination.z += 1f; // just to give it a little more life
	}


	public void Graph(float val)
	{
		trans.position = Vector3.Lerp(origin, destination, val);
	}
}
