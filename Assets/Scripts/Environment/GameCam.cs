﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Simple rotating camera
/// camera has two positions: Edit position and Play position
/// </summary>
public class GameCam : MonoBehaviour 
{
	public Transform target; // Thing to look at
	public float speed;
	public float transitionSpeed;
	public float editHeight;


	public float moveDamping = 1;
	public float maxSpeed = 2f;

	public float minZoom = 1.5f;
	public float maxZoom = 10f;

	public float zoomSpeed = .3f;
	public float zoomDamp = 5f;

	protected float curZoom = 70;
	protected float targetZoom;

	protected Vector3 prevAxis;
	protected Vector3 curAxis;


	protected Transform trans;
	protected Transform cam;

	protected Vector3 origin;
	protected Vector3 camOrigin;


	void Start ()
	{
		trans = transform;	// Direct references are always faster
		origin = trans.position;

		curAxis = prevAxis = Vector3.zero;

		cam = GetComponentInChildren<Camera>().transform;
		camOrigin = cam.transform.localPosition;
		cam.LookAt(target);
	}
	
	void Update () 
	{
		MouseMovement();
		trans.RotateAround(target.position, Vector3.up, speed);
	}

	/// <summary>
	/// Moves the camera into position for editing the petri dish
	/// </summary>
	/// <returns></returns>
	public bool MoveToEdit()
	{

		//cam.localPosition = Vector3.Lerp(cam.localPosition, Vector3.zero + (Vector3.up * 100), Time.fixedDeltaTime * transitionSpeed);
		//return Vector3.Distance(cam.localPosition, Vector3.zero + (Vector3.up * 100)) < .01f;
		return true;
	}

	/// <summary>
	/// Moves camera into position to watch the simulation unfold
	/// </summary>
	/// <returns></returns>
	public bool MoveToPlay()
	{

		//cam.localPosition = Vector3.Lerp(cam.localPosition, camOrigin, Time.fixedDeltaTime * transitionSpeed);
		//return Vector3.Distance(cam.localPosition, camOrigin) < .01f;
		return true;
	}

	protected void MouseMovement()
	{
		if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift))
			curAxis = Input.mousePosition;

		if (Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftShift))
		{
			prevAxis = curAxis;
			curAxis.x = Input.mousePosition.x;
			curAxis.y = Input.mousePosition.y;
		}
		else
			prevAxis = Vector3.Lerp(prevAxis, curAxis, Time.fixedDeltaTime * moveDamping);

		Vector3 rot = curAxis - prevAxis;
		trans.RotateAround(Vector3.zero, Vector3.up, MapClamp(rot.x, -10, 10, -maxSpeed, maxSpeed));
		trans.RotateAround(Vector3.zero, trans.right, MapClamp(rot.y, -10, 10, -maxSpeed, maxSpeed));

		float wheel = Input.GetAxis("mousewheel");
		if (wheel != 0)
			targetZoom = wheel < 0 ? maxZoom : -maxZoom;

		targetZoom = Mathf.Lerp(targetZoom, 0, Time.fixedDeltaTime * zoomDamp);

		curZoom = Mathf.Clamp(Mathf.Lerp(curZoom, curZoom + targetZoom, Time.fixedDeltaTime * zoomSpeed), minZoom, maxZoom);

		cam.camera.orthographicSize = curZoom;
	}

	protected float MapClamp(float f, float inMin, float inMax, float outMin, float outMax)
	{
		return Mathf.Clamp((((outMax - outMin) * (f - inMin)) / (inMax - inMin) + outMin), outMin, outMax);
	}

}
