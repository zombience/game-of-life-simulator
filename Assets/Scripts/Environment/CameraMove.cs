﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour
{

	public float moveDamping = 1;
	public float maxSpeed = 2f;

	public float minZoom = 1.5f;
	public float maxZoom = 10f;

	public float zoomSpeed = .3f;
	public float zoomDamp = 5f;

	protected float curZoom = 70;
	protected float targetZoom;

	protected Vector3 prevAxis;
	protected Vector3 curAxis;

	protected Transform trans;
	protected Transform cam;

	void Start()
	{
		trans = transform;
		curAxis = prevAxis = Vector3.zero;
		cam = trans.GetChild(0);
	}

	void Update()
	{

		if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift))
			curAxis = Input.mousePosition;

		if (Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftShift))
		{
			prevAxis = curAxis;
			curAxis.x = Input.mousePosition.x;
			curAxis.y = Input.mousePosition.y;
		}
		else
			prevAxis = Vector3.Lerp(prevAxis, curAxis, Time.fixedDeltaTime * moveDamping);

		Vector3 rot = curAxis - prevAxis;
		trans.RotateAround(Vector3.zero, Vector3.up, MapClamp(rot.x, -10, 10, -maxSpeed, maxSpeed));
		trans.RotateAround(Vector3.zero, trans.right, MapClamp(rot.y, -10, 10, -maxSpeed, maxSpeed));

		float wheel = Input.GetAxis("mousewheel");
		if (wheel != 0)
			targetZoom = wheel < 0 ? maxZoom : -maxZoom;

		targetZoom = Mathf.Lerp(targetZoom, 0, Time.fixedDeltaTime * zoomDamp);

		curZoom = Mathf.Clamp(Mathf.Lerp(curZoom, curZoom + targetZoom, Time.fixedDeltaTime * zoomSpeed), minZoom, maxZoom);

		cam.camera.orthographicSize = curZoom;
	}

	public float MapClamp(float f, float inMin, float inMax, float outMin, float outMax)
	{
		return Mathf.Clamp((((outMax - outMin) * (f - inMin)) / (inMax - inMin) + outMin), outMin, outMax);
	}

}
